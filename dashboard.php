<?php
    session_start();
    include('includes/check_user.php');
    include('includes/config.php');

    if ($_SESSION['utype']!="A") 
    {
        header('Location:profile.php?page=1');
    }
?>


<!doctype html>
<html class="no-js " lang="en">
<head>
<?php include('includes/meta.php'); ?>
<?php include('includes/title.php'); ?>
<?php include('includes/favicon.php'); ?>
<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
<!-- <link rel="stylesheet" href="assets/plugins/jvectormap/jquery-jvectormap-2.0.3.min.css"/> -->
<!-- <link rel="stylesheet" href="assets/plugins/morrisjs/morris.min.css" /> -->
<!-- Custom Css -->
<link rel="stylesheet" href="assets/css/main.css">
<link rel="stylesheet" href="assets/css/color_skins.css">
</head>
<body class="theme-purple">
<!-- Page Loader -->
<?php include('includes/preloader.php'); ?>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<?php include('includes/top_navbar.php'); ?>
<?php include('includes/left_sidebar.php'); ?>

<!-- Main Content -->
<section class="content home">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Dashboard
                <?php include('includes/sub_title.php'); ?>
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12"> 
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.php"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                </ul>                
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="body">
                        <?php
                            $data = "SELECT COUNT(*) as c FROM user";
                            $res = mysql_query($data,$conn);
                            $row=mysql_fetch_assoc($res);
                        ?>
                        <h3 class="number count-to" data-from="0" data-to="<?=$row['c']?>" data-speed="5000" data-fresh-interval="700" >
                        <?=$row['c']?>
                        </h3>                        
                        <p class="text-muted">Total Users</p>
                        <div class="progress">
                            <div class="progress-bar l-blue" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
                        </div>
                        <small>Change 27%</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="body">
                        <?php
                            $data = "SELECT COUNT(*) as c FROM user WHERE status='A'";
                            $res = mysql_query($data,$conn);
                            $row=mysql_fetch_assoc($res);
                        ?>
                        <h3 class="number count-to" data-from="0" data-to="758" data-speed="2000" data-fresh-interval="700" ><?=$row['c']?></h3>
                        <p class="text-muted">Active Users</p>
                        <div class="progress">
                            <div class="progress-bar l-green" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100" style="width: 68%;"></div>
                        </div>
                        <small>Change 9%</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="body">
                        <?php
                            $data = "SELECT COUNT(*) as c FROM user WHERE status='A' AND gender='Male' ";
                            $res = mysql_query($data,$conn);
                            $row=mysql_fetch_assoc($res);
                        ?>
                        <h3 class="number count-to" data-from="0" data-to="2521" data-speed="2000" data-fresh-interval="700" ><?=$row['c']?></h3>
                        <p class="text-muted">Male Users</p>
                        <div class="progress">
                            <div class="progress-bar l-amber" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100" style="width: 68%;"></div>
                        </div>
                        <small>Change 17%</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card">
                     <?php
                            $data = "SELECT COUNT(*) as c FROM user WHERE status='A' AND gender='FeMale' ";
                            $res = mysql_query($data,$conn);
                            $row=mysql_fetch_assoc($res);
                        ?>
                    <div class="body">
                        <h3><?=$row['c']?></h3>
                        <p class="text-muted">Female Users</p>
                        <div class="progress">
                            <div class="progress-bar l-parpl" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100" style="width: 68%;"></div>
                        </div>
                        <small>Change 13%</small>
                    </div>
                </div>
            </div>            
        </div>
        
        
        
        <div class="row clearfix" style="display: none;">
            <div class="col-lg-3 col-md-6 col-sm-12 text-center">
                <div class="card tasks_report">
                    <div class="body">
                        <input type="text" class="knob dial2" value="66" data-width="90" data-height="90" data-thickness="0.1" data-fgColor="#4CAF50" readonly>
                        <h6 class="m-t-20">Apartment</h6>
                        <p class="displayblock m-b-0">29% Average <i class="zmdi zmdi-trending-up"></i></p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 text-center">
                <div class="card tasks_report">
                    <div class="body">
                        <input type="text" class="knob dial2" value="26" data-width="90" data-height="90" data-thickness="0.1" data-fgColor="#7b69ec" readonly>
                        <h6 class="m-t-20">Office</h6>
                        <p class="displayblock m-b-0">45% Average <i class="zmdi zmdi-trending-down"></i></p>
                        
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 text-center">
                <div class="card tasks_report">
                    <div class="body">
                        <input type="text" class="knob dial3" value="76" data-width="90" data-height="90" data-thickness="0.1" data-fgColor="#f9bd53" readonly>
                        <h6 class="m-t-20">Shop</h6>
                        <p class="displayblock m-b-0">75% Average <i class="zmdi zmdi-trending-up"></i></p>
                        
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 text-center">
                <div class="card tasks_report">
                    <div class="body">
                        <input type="text" class="knob dial4" value="88" data-width="90" data-height="90" data-thickness="0.1" data-fgColor="#00adef" readonly>
                        <h6 class="m-t-20">Villa</h6>
                        <p class="displayblock m-b-0">12% Average <i class="zmdi zmdi-trending-up"></i></p>
                        
                    </div>
                </div>
            </div>            
        </div>
          
    </div>
</section>
<!-- Jquery Core Js --> 
<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js ( jquery.v3.2.1, Bootstrap4 js) --> 
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- slimscroll, waves Scripts Plugin Js -->

<!-- <script src="assets/bundles/morrisscripts.bundle.js"></script> --><!-- Morris Plugin Js -->
<!-- <script src="assets/bundles/jvectormap.bundle.js"></script> --> <!-- JVectorMap Plugin Js -->
<!-- <script src="assets/bundles/knob.bundle.js"></script> --> <!-- Jquery Knob Plugin Js -->
<!-- <script src="assets/bundles/countTo.bundle.js"></script> --> <!-- Jquery CountTo Plugin Js -->
<!-- <script src="assets/bundles/sparkline.bundle.js"></script> --> <!-- Sparkline Plugin Js -->

<script src="assets/bundles/mainscripts.bundle.js"></script>
<script src="assets/js/pages/index.js"></script>
</body>
</html>