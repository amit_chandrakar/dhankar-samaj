<?php
    session_start();
    include('includes/check_user.php');
    include('includes/config.php');
?>
<!doctype html>
<html class="no-js " lang="en">
<head>
<?php include('includes/meta.php'); ?>
<?php include('includes/title.php'); ?>
<?php include('includes/favicon.php'); ?>
<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
<!-- Custom Css -->
<link rel="stylesheet" href="assets/css/main.css">
<link rel="stylesheet" href="assets/css/color_skins.css">
</head>
<body class="theme-purple">
<!-- Page Loader -->
<?php include('includes/preloader.php'); ?>

<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<?php include('includes/top_navbar.php'); ?>
<?php include('includes/left_sidebar.php'); ?>

<!-- Main Content -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>About Community
                <?php include('includes/sub_title.php'); ?>
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">         
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                    <li class="breadcrumb-item active">About Community</li>
                </ul>                
            </div>
            
        </div>

    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="body">
                        <center><h1>Proud to be gadriya.</h1></center>
                        <center><img src="sheep3.png" class="img-responsive"></center>
                    </div>

                    <div><br></div>
                    <div class="row">
                        <div class="col-lg-1"> </div>  
                        <div class="col-lg-8 col-md-8 col-sm-12">   
                        <h4>Developed By</h4>
                            <span>Amit Chandrakar</span><br>
                            <span>Whatsapp No : <a href="https://api.whatsapp.com/send?phone=918839826466">8839826466</a></span> <br>
                            <span>Call : <a href="tel:8839826466">8839826466</a></span>      
                        </div>
                        <div class="col-lg-3 col-md4 col-sm-4">      
                            <h4>Guided By</h4>
                            <span>Devendra Dhankar</span><br>
                            <span>Whatsapp No : <a href="https://api.whatsapp.com/send?phone=8770546442">8770546442</a></span> <br>
                            <span>Call : <a href="tel:8770546442">8770546442</a></span>   
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12">  <br>       
                        </div>
                    </div>
                    

                </div>
            </div>
        </div>
    </div>
</section>
<!-- Jquery Core Js --> 
<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
<script src="assets/bundles/mainscripts.bundle.js"></script>
</body>
</html>