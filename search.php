<?php
    session_start();
    include('includes/check_user.php');
    include('includes/config.php');
?>
<!doctype html>
<html class="no-js " lang="en">
<head>
<?php include('includes/meta.php'); ?>
<?php include('includes/title.php'); ?>
<?php include('includes/favicon.php'); ?>
<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<!-- Custom Css -->
<link rel="stylesheet" href="assets/css/main.css"> 
<link rel="stylesheet" href="assets/css/color_skins.css">
</head>
<body class="theme-purple">
<!-- Page Loader -->
<?php include('includes/preloader.php'); ?>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>

<?php include('includes/top_navbar.php'); ?>
<?php include('includes/left_sidebar.php'); ?>

<!-- Main Content -->
<section class="content home">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Search
                <small class="text-muted">Welcome to Compass</small>
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="dashboard.php"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                    <li class="breadcrumb-item active">Search</li>
                </ul>                
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">
                        <h2>Search</h2>
                    </div>
                    <div class="body">
                        <form action="search.php">
                        <div class="row clearfix">
                            <input type="hidden" name="gender" value="<?=$_REQUEST['gender']?>">
                           
                            <div class="col-sm-3">
                                <label>Category</label>
                                <select class="form-control show-tick" name="search_age">
                                    <option value="">-- Select --</option>
                                    <option value="Jheriya">Jheriya</option>
                                    <option value="Dhengar">Dhengar</option>
                                    <option value="Jhade">Jhade</option>
                                    <option value="Varade">Varade</option>
                                    <option value="Desha">Desha</option>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <label>Age Between</label>
                                <select class="form-control show-tick" name="search_age">
                                    <option value="">-- Select --</option>
                                    <option value="20">20-25</option>
                                    <option value="25">25-30</option>
                                    <option value="30">30-35</option>
                                    <option value="35">35-40</option>
                                    <option value="40">Above 40</option>
                                </select>
                            </div>
                            <!-- <div class="col-sm-3">
                                <label>Having Gotra</label>
                                <select class="form-control show-tick" name="search_age">
                                    <option value="">-- Select --</option>
                                    <option value="20">20-25</option>
                                    <option value="25">25-30</option>
                                    <option value="30">30-35</option>
                                    <option value="35">35-40</option>
                                    <option value="40">Above 40</option>
                                </select>
                            </div> -->
                            <div class="col-sm-3">
                                <label>State</label>
                                <select class="form-control show-tick" name="search_marital_status" onchange="change_city(this.value)">
                                    <option value="">--select--</option>
                                    <?php
                                    $sql=mysql_query("select * from state",$conn);
                                    while ($row=mysql_fetch_assoc($sql)) 
                                    {
                                      ?>
                                       <option value="<?=$row['stateid']?>"><?=$row['statename']?></option>
                                      <?php
                                    } ?>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <label>City</label>
                                <span id="span_adrsc_city">
                                <select class="form-control show-tick" name="search_marital_status">
                                    <option value="">--select--</option>
                                    <?php
                                    $sql=mysql_query("select * from city",$conn);
                                    while ($row=mysql_fetch_assoc($sql)) 
                                    {
                                      ?>
                                       <option value="<?=$row['cityid']?>"><?=$row['cityname']?></option>
                                      <?php
                                    } ?>
                                </select>
                                </span>
                            </div>
                            <div class="col-sm-3">
                                <button type="submit" class="btn btn-round btn-primary waves-effect">Search</button>
                                <button type="Reset" class="btn btn-round btn-primary waves-effect">Reset</button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">



                <?php
                if (isset($_REQUEST['gender'])  && isset($_REQUEST['search_age']) && isset($_REQUEST['search_marital_status']) && $_REQUEST['gender']!="" && $_REQUEST['gender']!="All"  && $_REQUEST['search_age']!="" && $_REQUEST['search_marital_status']!="") 
                {
                    $age_from = $_REQUEST['search_age'];
                    $age_to = $age_from+5;
                    $data = "SELECT * FROM user WHERE utype!='A' AND status!='D' AND gender='".$_REQUEST['gender']."' AND married = '".$_REQUEST['search_marital_status']."' AND age between '$age_from' AND '$age_to' ";
                }
                elseif (isset($_REQUEST['gender']) && $_REQUEST['gender']!='' && $_REQUEST['gender']!='All') 
                {
                    $data = "SELECT * FROM user WHERE utype!='A' AND status!='D' AND gender='".$_REQUEST['gender']."' ";
                }
                else
                {
                    $data = "SELECT * FROM user WHERE  utype!='A' AND status!='D'";
                }
                $res = mysql_query($data,$conn);
                if (mysql_num_rows($res)>0) 
                {

                while ($row=mysql_fetch_assoc($res)) 
                { ?>

                    <div class="card property_list">
                    <div class="body">
                        <div class="row">
                            <div class="col-lg-4 col-md-6">
                                <div class="property_image">
                                    <a href="person_detail.php?userid=<?=$row['userid']?>"><img style="height: 180px; width: 360px; " class="img-thumbnail img-fluid" src="<?php if(file_exists('uploads/'.$row['userid'].'/'.$row['photo'])){echo 'uploads/'.$row['userid'].'/'.$row['photo'];}else{echo 'assets/images/image-gallery/7.jpg';} ?>" alt="img"></a>
                                    <span class="badge badge-danger">
                                        <?=$row['gender']?>
                                        </span>
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-6">
                                <div class="property-content">
                                    <div class="detail">
                                        <h5 class="text-success m-t-0 m-b-0">
                                            <!-- <?=$row['total_amt']?> -->
                                               <?=ucwords($row['fname'])," ",ucwords($row['mname'])," ",ucwords($row['lname'])?>
                                            </h5>
                                        <h4 class="m-t-0"><a href="person_detail.php?userid=<?=$row['userid']?>" class="col-blue-grey">
                                            <?=ucwords(strtolower($row['post']))," At ",ucwords(strtolower($row['company']))?>
                                        </a></h4>
                                        <p class="text-muted"><i class="zmdi zmdi-pin m-r-5"></i>
                                            <?php if($row['location']==""){echo "Not Available";}else{echo $row['location'];}?>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="zmdi zmdi-account-calendar"></i> 
                                            <?php 
                                                $d1 = new DateTime(date('Y-m-d'));
                                                $d2 = new DateTime($row['dob']);
                                                $diff = $d2->diff($d1);
                                                echo $diff->y;
                                                // if($row['age']==0)
                                                //     {
                                                //         echo "0";
                                                //     }
                                                // else
                                                //     {
                                                //         echo $row['age'];
                                                //     }
                                            ?> years old
                                        </p>
                                        <p class="text-muted m-b-0"><?php if($row['description']==""){echo "Person description Not Available";}else{echo $row['description'];}?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

          <?php }
            }
            else
            { ?>

                <div class="card property_list">
                    <div class="body">
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <center><h1>No Data Found..!</h1></center>
                            </div>
                        </div>
                    </div>
                </div>



         <?php } ?>

            </div>

        </div>
    </div>
</section>
<!-- Jquery Core Js --> 
<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js ( jquery.v3.2.1, Bootstrap4 js) --> 
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- slimscroll, waves Scripts Plugin Js -->
<script src="assets/bundles/mainscripts.bundle.js"></script>
</body>
</html>
<script type="text/javascript">
    function change_city(stateid)
    {
      $.post("Ajax.php", {action:'get_current_city', stateid:stateid}, function(data){
        $("#span_adrsc_city").html(data);
      });
    }
</script>