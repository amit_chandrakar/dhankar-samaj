<?php
    session_start();
    include('includes/check_user.php');
    include('includes/config.php');
    if (isset($_REQUEST['approve_user']) && $_REQUEST['approve_user']!="") 
    {
        $approve_user = "UPDATE user SET status='A' WHERE userid=".$_REQUEST['approve_user'];
        $query = mysql_query($approve_user, $conn);
        if ($query) 
        {
            echo "<script>alert('user activated..!')</script>";
            echo "<script>window.location='users.php';</script>";
        }
    }
    elseif (isset($_REQUEST['disable_user']) && $_REQUEST['disable_user']!="") 
    {
        $disable_user = "UPDATE user SET status='D' WHERE userid=".$_REQUEST['disable_user'];
        $query = mysql_query($disable_user, $conn);
        if ($query) 
        {
            echo "<script>alert('User Disabled')</script>";
        }
    }
    elseif (isset($_REQUEST['delete_user']) && $_REQUEST['delete_user']!="") 
    {
        $disable_user = "DELETE FROM user WHERE userid=".$_REQUEST['delete_user'];
        $query = mysql_query($disable_user, $conn);
        if ($query) 
        {
            echo "<script>alert('User Deleted')</script>";
            echo "<script>window.location='users.php';</script>";
        }
    }






?>
<!doctype html>
<html class="no-js " lang="en">
<head>
<?php include('includes/meta.php'); ?>
<?php include('includes/title.php'); ?>
<?php include('includes/favicon.php'); ?>
<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
<!-- Custom Css -->
<link rel="stylesheet" href="assets/css/main.css">
<link rel="stylesheet" href="assets/css/color_skins.css">
</head>
<body class="theme-purple">
<!-- Page Loader -->
<?php include('includes/preloader.php'); ?>

<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<?php include('includes/top_navbar.php'); ?>
<?php include('includes/left_sidebar.php'); ?>

<!-- Main Content -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>User Description
                <?php include('includes/sub_title.php'); ?>
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">         
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                    <li class="breadcrumb-item"><a href=""><i class="zmdi zmdi-accounts-outline"></i> Users</a></li>
                    <li class="breadcrumb-item active">User Description</li>
                </ul>                
            </div>
        </div>
    </div>
    <?php
        $data = "SELECT * FROM user WHERE userid=".$_REQUEST['userid'];
        $res = mysql_query($data,$conn);
        $row=mysql_fetch_assoc($res);
    ?>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="body">
                        <div class="table-responsive">
                        <table width="100%" class="table table-bordered table-striped">
                            <tr>
                                <td colspan="2"><img width="100%" src="<?php if($row['photo']!="" && file_exists("uploads/".$row['userid']."/".$row['photo'])){echo"uploads/".$row['userid']."/".$row['photo'];}else{echo "assets/images/xs/avatar1.jpg";} ?> " class="img img-thumbnail img-responsive"></td>
                            </tr>
                            <tr>
                                <td>Name</td>
                                <td><?=$row['fname']," ",$row['mname']," ",$row['lname']?></td>
                            </tr>
                            <tr>
                                <td>Gender</td>
                                <td><?=$row['gender']?></td>
                            </tr>
                            <tr>
                                <td>Date of Birth</td>
                                <td><?=date('d-m-Y', strtotime($row['dob']))?></td>
                            </tr>
                            <tr>
                                <td>Age</td>
                                <td><?=$row['age']?> Year</td>
                            </tr>
                            <tr>
                                <td>Contact</td>
                                <td><?=$row['contact']?></td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td><?=$row['email']?></td>
                            </tr>
                            <tr>
                                <td>Father's Name</td>
                                <td><?=$row['father']?></td>
                            </tr>
                            <tr>
                                <td>Father's Occupation</td>
                                <td><?=$row['f_post']?></td>
                            </tr>
                            <tr>
                                <td>Father's Gotra</td>
                                <td><?=$row['pita_gotra']?></td>
                            </tr>
                            <tr>
                                <td>Mother's Name</td>
                                <td><?=$row['mother']?></td>
                            </tr>
                            <tr>
                                <td>Mother's Occupation</td>
                                <td><?=$row['m_post']?></td>
                            </tr>
                            <tr>
                                <td>Mother's Gotra</td>
                                <td><?=$row['mata_gotra']?></td>
                            </tr>
                            <tr>
                                <td>Marital Status</td>
                                <td><?=$row['married']?></td>
                            </tr>
                            <tr>
                                <td>Highest Education</td>
                                <td><?=$row['quali']?></td>
                            </tr>
                            <tr>
                                <td>Specilize</td>
                                <td><?=$row['stream']?></td>
                            </tr>
                            <tr>
                                <td>Company Name</td>
                                <td><?=$row['company']?></td>
                            </tr>
                            <tr>
                                <td>Post</td>
                                <td><?=$row['post']?></td>
                            </tr>
                            <tr>
                                <td>Salary</td>
                                <td>&#x20b9; <?=$row['salary']?></td>
                            </tr>
                            <tr>
                                <td>Country</td>
                                <td>
                                    <?php
                                        $country = "SELECT * FROM country WHERE countryid=".$row['country'];
                                        $query = mysql_query($country, $conn);
                                        $r = mysql_fetch_assoc($query);
                                        echo $r['countryname']
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>State</td>
                                <td>
                                    <?php
                                        $country = "SELECT * FROM state WHERE stateid=".$row['state'];
                                        $query = mysql_query($country, $conn);
                                        $r = mysql_fetch_assoc($query);
                                        echo $r['statename']
                                    ?>    
                                </td>
                            </tr>
                            <tr>
                                <td>City</td>
                                <td>
                                    <?php
                                        $country = "SELECT * FROM city WHERE cityid=".$row['city'];
                                        $query = mysql_query($country, $conn);
                                        $r = mysql_fetch_assoc($query);
                                        echo $r['cityname']
                                    ?>     
                                </td>
                            </tr>
                            <tr>
                                <td>Locality</td>
                                <td>
                                    <?=$row['locality']?>     
                                </td>
                            </tr>
                            <tr>
                                <td>Pin</td>
                                <td><?=$row['pin']?></td>
                            </tr>
                            <tr>
                                <td>Address</td>
                                <td><?=$row['location']?></td>
                            </tr>
                            <tr>
                                <td>Diet</td>
                                <td><?=$row['diet']?></td>
                            </tr>
                            <tr>
                                <td>Height (in foot)</td>
                                <td><?=$row['height']?></td>
                            </tr>
                            <tr>
                                <td>Smoke</td>
                                <td><?=$row['smoke']?></td>
                            </tr>
                            <tr>
                                <td>Drink</td>
                                <td><?=$row['drink']?></td>
                            </tr>
                            <tr>
                                <td>Physically Disability</td>
                                <td><?php if($row['physically_disable']==0){echo "No";}else{echo "Yes";}?></td>
                            </tr>
                        </table>
                        <center> 
                            <?php
                                if ($row['status']!="A") 
                                { ?>
                                    <button class="btn btn-success btn-round btn-simple" onclick="javascript:location.href='user_desc.php?userid=<?=$_REQUEST['userid']?>&approve_user=<?=$row['userid']?>'">Approve</button> 
                                    <button class="btn btn-danger btn-round btn-simple" onclick="javascript:location.href='user_desc.php?userid=<?=$_REQUEST['userid']?>&delete_user=<?=$row['userid']?>'">Reject</button> 
                          <?php }
                                else
                                { ?>
                                    <button class="btn btn-danger btn-round btn-simple" onclick="javascript:location.href='user_desc.php?userid=<?=$_REQUEST['userid']?>&disable_user=<?=$row['userid']?>'">Disable</button> 
                                    <button class="btn btn-warning btn-round btn-simple" onclick="javascript:location.href='user_desc.php?userid=<?=$_REQUEST['userid']?>&delete_user=<?=$row['userid']?>'">Delete User</button>
                          <?php }
                            ?>
                            
                             
                            <button class="btn btn-warning btn-round btn-simple" onclick="javascript:location.href='users.php'">Cancel</button> 

                        </center>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Jquery Core Js --> 
<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
<script src="assets/bundles/mainscripts.bundle.js"></script>
</body>
</html>