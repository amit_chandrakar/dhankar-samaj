﻿<?php 
    session_start();
    if (isset($_SESSION['login']))
    {
        header('Location: dashboard.php');
    }
    if (isset($_POST['submit'])) 
    {
        include "includes/config.php";
        $sql = mysql_query("SELECT * from user WHERE status='A' AND email='".$_POST['username']."' AND password='".$_POST['password']."' ", $conn);
         $row=mysql_fetch_assoc($sql);
        if(mysql_num_rows($sql)==1)
        { 
                $_SESSION['login']=true; 
                $_SESSION['userid']=$row['userid'];
                $_SESSION['utype']=$row['utype'];
                $_SESSION['status']=$row['status'];
                $_SESSION['fname']=$row['fname'];
                $_SESSION['mname']=$row['mname'];
                $_SESSION['lname']=$row['lname'];
                $_SESSION['post']=$row['post'];
                if ($row['utype']=="A") 
                {
                   header('Location:dashboard.php');
                }
                else
                {
                    header('Location:profile.php?page=1');
                }
        }
        else
        { 
            header('Location:index.php?fail=1');
        }
    }
?>


<!doctype html>
<html class="no-js " lang="en">
<head>
    <?php include('includes/meta.php'); ?>
    <?php include('includes/title.php'); ?>
    <?php include('includes/favicon.php'); ?>
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/authentication.css">
    <link rel="stylesheet" href="assets/css/color_skins.css">
</head>

<body class="theme-purple authentication sidebar-collapse">
<!-- End Navbar -->
<div class="page-header">
    <div class="page-header-image" style="background-image:url(reg/form-wizard-bg.jpg)"></div>
    <div class="container">
        <div class="col-md-12 content-center">
            <div class="card-plain">
                <form class="form" method="post" action="index.php">
                    <div class="header">
                        <div class="logo-container">
                            <img src="logo.png" alt="">
                        </div>
                        <h5>Log in</h5>
                        <?php
                        if (isset($_REQUEST['register']) && $_REQUEST['register']=='success') 
                        { ?>
                            <div class="alert alert-success alert-dismissible" id="success-alert">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>Register Success..!</strong>  Please login with your credentials.
                            </div>
                      <?php  }
                        elseif (isset($_REQUEST['register']) && $_REQUEST['register']=='fail') 
                        { ?>
                            <div class="alert alert-danger alert-dismissible" id="fail-alert">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>Register Failed..!</strong>  Please try again later.
                            </div>
                      <?php  }
                        elseif (isset($_REQUEST['fail']) && $_REQUEST['fail']==1) 
                        { ?>
                            <div class="alert alert-danger alert-dismissible" id="fail-alert">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>Invalid Username or Password..!</strong>.
                            </div>

                    <?php  }
                        ?>

                    </div>
                    <div class="content">                                                
                        <div class="input-group input-lg">
                            <input type="text" class="form-control" placeholder="Enter Email" name="username" autofocus autocomplete="off">
                            <span class="input-group-addon">
                                <i class="zmdi zmdi-account-circle"></i>
                            </span>
                        </div>
                        <div class="input-group input-lg">
                            <input type="password" placeholder="Password" class="form-control" name="password" autocomplete="off">
                            <span class="input-group-addon">
                                <i class="zmdi zmdi-lock"></i>
                            </span>
                        </div>
                    </div>
                    <div class="footer text-center">
                        <!-- <a href="dashboard.php" class="btn l-cyan btn-round btn-lg btn-block waves-effect waves-light">SIGN IN</a> -->
                        <input type="submit" name="submit" class="btn l-cyan btn-round btn-lg btn-block waves-effect waves-light" value="SIGN IN">
                        <h6 class="m-t-20"><a href="forgot_password.php" class="link">Forgot Password?</a></h6>
                        <h6 class="m-t-20"><a href="reg/" class="link">Not have an account? Register here</a></h6>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Jquery Core Js -->
<script src="assets/bundles/libscripts.bundle.js"></script>
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->

<script>
   $(".navbar-toggler").on('click',function() {
    $("html").toggleClass("nav-open");
});
//=============================================================================
$('.form-control').on("focus", function() {
    $(this).parent('.input-group').addClass("input-group-focus");
}).on("blur", function() {
    $(this).parent(".input-group").removeClass("input-group-focus");
});
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready (function(){
            $("#success-alert").fadeOut(5000);
            $("#fail-alert").fadeOut(5000);
 });

</script>

</body>
</html>