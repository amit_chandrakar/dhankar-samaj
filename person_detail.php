<?php
    session_start();
    include('includes/check_user.php');
    include('includes/config.php');
?>
<!doctype html>
<html class="no-js " lang="en">
<head>
<?php include('includes/meta.php'); ?>
<?php include('includes/title.php'); ?>
<?php include('includes/favicon.php'); ?>
<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
<!-- Custom Css -->
<link rel="stylesheet" href="assets/css/main.css">
<link rel="stylesheet" href="assets/css/color_skins.css">
</head>
<body class="theme-purple">
<!-- Page Loader -->
<?php include('includes/preloader.php'); ?>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>

<!-- Top Bar -->
<?php include('includes/top_navbar.php'); ?>
<?php include('includes/left_sidebar.php'); ?>

<!-- Main Content -->
<section class="content home">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Person Detail
                <?php include('includes/sub_title.php'); ?>
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">  
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                    <li class="breadcrumb-item active">Person Detail</li>
                </ul>                
            </div>
        </div>
    </div>
    <?php
        $data = "SELECT * FROM user WHERE userid=".$_REQUEST['userid'];
        $res = mysql_query($data,$conn);
        $row=mysql_fetch_assoc($res);
        $path = 'uploads/'.$row['userid'].'/'.$row['photo'];
    ?>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-8 col-md-12">
                <div class="card">
                    <div class="body">
                        <img style="height: 100%; width: 100%; " class="img-thumbnail img-fluid" src="<?php if(file_exists($path)){echo 'uploads/'.$row['userid'].'/'.$row['photo'];}else{echo 'assets/images/image-gallery/7.jpg';} ?>" alt="img">
                    </div>
                </div>

                

                <div class="card property_list">
                    <div class="body">
                        <div class="property-content">
                            <div class="detail">
                                <h4 class="text-success m-t-0 m-b-0"><?=$row['fname']," ",$row['mname']," ",$row['lname']?></h4>
                                <h4 class="m-t-0"><a href="#" class="col-blue-grey"><?=$row['post']?></a></h4>
                                <p class="text-muted"><i class="zmdi zmdi-pin m-r-5"></i><?=$row['location']?></p>
                                <p class="text-muted m-b-0"><?=$row['description']?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card property_list">
                    <div class="body">
                        <div class="row property-content">
                            <div class="detail col-lg-6">
                                <h5 class=" m-t-0 m-b-0">Father's Name : <?=$row['father']?></h5>
                                <h5 class="m-t-0 m-b-0">Father's Occupation : <?=$row['f_post']?></h5>
                                <h5 class="m-t-0 m-b-0">Father's Gotra :  <?=$row['pita_gotra']?> </h5>
                            </div>
                            <div class="detail col-lg-6">
                                <h5 class=" m-t-0 m-b-0">Mother's Name : <?=$row['mother']?></h5>
                                <h5 class="m-t-0 m-b-0">Mother's Occupation :  <?=$row['f_post']?></h5>
                                <h5 class="m-t-0 m-b-0">Mother's Gotra :  <?=$row['mata_gotra']?></h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card" style="display: none;">
                    <div class="header">
                        <h2><strong>General</strong> Amenities<small >Description Text Here...</small></h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-sm-4">
                                <ul class="list-unstyled proprerty-features">
                                <li><i class="zmdi zmdi-check-circle text-success m-r-5"></i>Swimming pool</li>
                                <li><i class="zmdi zmdi-check-circle text-success m-r-5"></i>Air conditioning</li>
                                <li><i class="zmdi zmdi-check-circle text-success m-r-5"></i>Internet</li>
                                <li><i class="zmdi zmdi-check-circle text-success m-r-5"></i>Radio</li>
                                <li><i class="zmdi zmdi-check-circle text-success m-r-5"></i>Balcony</li>
                                <li><i class="zmdi zmdi-check-circle text-success m-r-5"></i>Roof terrace</li>
                                <li><i class="zmdi zmdi-check-circle text-success m-r-5"></i>Cable TV</li>
                                <li><i class="zmdi zmdi-check-circle text-success m-r-5"></i>Electricity</li>
                            </ul>
                            </div>
                            <div class="col-sm-4">
                                <ul class="list-unstyled proprerty-features">
                                    <li><i class="zmdi zmdi-star text-warning m-r-5"></i>Terrace</li>
                                    <li><i class="zmdi zmdi-star text-warning m-r-5"></i>Cofee pot</li>
                                    <li><i class="zmdi zmdi-star text-warning m-r-5"></i>Oven</li>
                                    <li><i class="zmdi zmdi-star text-warning m-r-5"></i>Towelwes</li>
                                    <li><i class="zmdi zmdi-star text-warning m-r-5"></i>Computer</li>
                                    <li><i class="zmdi zmdi-star text-warning m-r-5"></i>Grill</li>
                                    <li><i class="zmdi zmdi-star text-warning m-r-5"></i>Parquet</li>
                                </ul>
                            </div>
                            <div class="col-sm-4">
                                <ul class="list-unstyled proprerty-features">
                                    <li><i class="zmdi zmdi-check-circle text-info m-r-5"></i>Dishwasher</li>
                                    <li><i class="zmdi zmdi-check-circle text-info m-r-5"></i>Near Green Zone</li>
                                    <li><i class="zmdi zmdi-check-circle text-info m-r-5"></i>Near Church</li>
                                    <li><i class="zmdi zmdi-check-circle text-info m-r-5"></i>Near Hospital</li>
                                    <li><i class="zmdi zmdi-check-circle text-info m-r-5"></i>Near School</li>
                                    <li><i class="zmdi zmdi-check-circle text-info m-r-5"></i>Near Shop</li>
                                    <li><i class="zmdi zmdi-check-circle text-info m-r-5"></i>Natural Gas</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Details</strong></h2>
                        <ul class="header-dropdown">
                            <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                <ul class="dropdown-menu dropdown-menu-right slideUp float-right">
                                    <li><a href="javascript:void(0);">Edit</a></li>
                                    <li><a href="javascript:void(0);">Delete</a></li>
                                    <li><a href="javascript:void(0);">Report</a></li>
                                </ul>
                            </li>
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">                        
                        <div class="table-responsive">
                            <table class="table table-bordered m-b-0">
                                <tbody>
                                    <tr>
                                        <th scope="row">Gender:</th>
                                        <td><?=$row['gender']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Category:</th>
                                        <td><?=$row['category']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Country: </th>
                                        <td>
                                            <?php
                                                $country = "SELECT * FROM country WHERE countryid=".$row['country'];
                                                $query = mysql_query($country, $conn);
                                                $r = mysql_fetch_assoc($query);
                                                echo $r['countryname']
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">State:</th>
                                        <td>
                                            <?php
                                                $country = "SELECT * FROM state WHERE stateid=".$row['state'];
                                                $query = mysql_query($country, $conn);
                                                $r = mysql_fetch_assoc($query);
                                                echo $r['statename']
                                            ?> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">City:</th>
                                        <td>
                                            <?php
                                                $country = "SELECT * FROM city WHERE cityid=".$row['city'];
                                                $query = mysql_query($country, $conn);
                                                $r = mysql_fetch_assoc($query);
                                                echo $r['cityname']
                                            ?> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Locality:</th>
                                        <td><?=$row['locality']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Qualification:</th>
                                        <td><?=$row['quali']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Stream:</th>
                                        <td><?=$row['stream']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Company:</th>
                                        <td><?=$row['company']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Post:</th>
                                        <td><?=$row['post']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Salary:</th>
                                        <td>&#x20b9; <?=$row['salary']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Diet:</th>
                                        <td><?=$row['diet']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Height:</th>
                                        <td><?=$row['height']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Smoke:</th>
                                        <td><?=$row['smoke']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Drink:</th>
                                        <td><?=$row['drink']?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Physically Disable:</th>
                                        <td><?php if($row['physically_disable']==0){echo "No";}else{echo "Yes";}?></td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Jquery Core Js --> 
<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js ( jquery.v3.2.1, Bootstrap4 js) --> 
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- slimscroll, waves Scripts Plugin Js -->

<script src="assets/bundles/mainscripts.bundle.js"></script>
</body>
</html>