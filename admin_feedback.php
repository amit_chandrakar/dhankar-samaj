<?php
    session_start();
    include('includes/check_user.php');
    include('includes/config.php');
?>
<!doctype html>
<html class="no-js " lang="en">
<head>
<?php include('includes/meta.php'); ?>
<?php include('includes/title.php'); ?>
<link rel="icon" href="favicon.ico" type="image/x-icon">
<!-- Favicon-->
<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
<!-- Custom Css -->
<link rel="stylesheet" href="assets/css/main.css">
<link rel="stylesheet" href="assets/css/color_skins.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">

</head>

<body class="theme-purple">
<!-- Page Loader -->
<?php include('includes/preloader.php'); ?>

<!-- Overlay For Sidebars -->
<div class="overlay"></div>

<?php include('includes/top_navbar.php'); ?>
<?php include('includes/left_sidebar.php'); ?>



<section class="content contact">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Feedback & Suggestion
                <?php include('includes/sub_title.php'); ?>
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="dashboard.php"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                    <li class="breadcrumb-item active">Feedback & Suggestion</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                
            </div>           
        </div>
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="body table-responsive">
                        <table class="table table-hover table-striped table-bordered" id="example" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Type</th>                                    
                                    <th data-breakpoints="xs">Title</th>
                                    <th data-breakpoints="xs sm md"> Description</th>
                                    <th data-breakpoints="xs sm md">Created By</th>
                                    <th data-breakpoints="xs sm md">Date</th>
                                    <th data-breakpoints="xs sm md">Action</th>
                                </tr>
                            </thead>
                                <tbody>

                                    <?php
                                     $data = "SELECT * FROM feedback ";
                                    $res = mysql_query($data,$conn);
                                    while ($row=mysql_fetch_assoc($res)) 
                                    { ?>
                                        <tr>
                                    <td>
                                        <span class="c_name"><?=$row['type']?></span>
                                    </td>
                                    <td>
                                        <span class="phone"><?=$row['title']?>
                                        </span>
                                    </td>
                                    <td>
                                        <span class="email"><?=$row['description']?></span>
                                    </td>
                                    <td>
                                        <span class="email">
                                            <?php 
                                                $user = "SELECT * FROM user where userid=".$row['created_by'];

                                                $rr = mysql_query($user,$conn);
                                                $tt = mysql_fetch_assoc($rr);
                                                echo $tt['fname']," ",$tt['mname']," ",$tt['lname'];
                                                
                                            ?>
                                                
                                            </span>
                                    </td>
                                     <td>
                                        <span class="email"><?=date('d-M-Y h:i a', strtotime($row['created_at'])) ?></span>
                                    </td>
                                    <td>
                                        <span> 
                                            <button class="btn btn-default btn-icon btn-simple btn-icon-mini btn-round" onclick="javascript:location.href=''"><i class="zmdi zmdi-eye"></i></button>
                                        </span>
                                    </td>
                                </tr>
                                  <?php } ?>

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Type</th>                                    
                                    <th data-breakpoints="xs">Title</th>
                                    <th data-breakpoints="xs sm md"> Description</th>
                                    <th data-breakpoints="xs sm md">Created By</th>
                                    <th data-breakpoints="xs sm md">Date</th>
                                    <th data-breakpoints="xs sm md">Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
  
    </div>
</section>
<!-- Jquery Core Js -->
<script src="assets/bundles/libscripts.bundle.js"></script>
<script src="assets/bundles/vendorscripts.bundle.js"></script>
<!-- <script src="assets/bundles/footable.bundle.js"></script> --> <!-- Lib Scripts Plugin Js -->
<script src="assets/bundles/mainscripts.bundle.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
    $('#example').DataTable();
} );
</script>
</body>
</html>