﻿<?php
    session_start();
    include('includes/check_user.php');
    include('includes/config.php');
    $userid = $_SESSION['userid'];

    if(isset($_POST['feedback']))
    {
        $title = $_POST['title'];
        $ddd = $_POST['ddd'];
        $type = $_POST['type'];
        $sql = "INSERT INTO `feedback`(`type`,`title`,`description`,`created_by`) VALUES('$type','$title', '$ddd','$userid')";
            
            $up=mysql_query($sql,$conn);
            if($up)
            {
                echo '<script>alert("data Inserted successfully!")</script>';
            }
    }


?>
<!doctype html>
<html class="no-js " lang="en">
<head>
<?php include('includes/meta.php'); ?>
<?php include('includes/title.php'); ?>
<?php include('includes/favicon.php'); ?>
<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">

<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<!-- Custom Css -->
<link rel="stylesheet" href="assets/css/main.css">
<link rel="stylesheet" href="assets/css/color_skins.css">
</head>
<body class="theme-purple" onload="enable();">
<!-- Page Loader -->
<?php include('includes/preloader.php'); ?>

<!-- Overlay For Sidebars -->
<div class="overlay"></div>

<?php include('includes/top_navbar.php'); ?>
<?php include('includes/left_sidebar.php'); ?>

<!-- Main Content -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Feedback & Suggestions
                <?php include('includes/sub_title.php'); ?>
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12"> 
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="dashboard.php"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                    <li class="breadcrumb-item active">Feedback & Suggestions</li>
                </ul>                
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="body">
                        <form action="feedback.php" method="post">
                        <div class="col-lg-12 col-md-12">
                            <label>Feedback Type</label>
                            <div class="form-group" style="margin-left: -15px;margin-top: -17px">
                                <select class="form-control show-tick" name="type" id="type" required>
                                <option value="">-- Select --</option>
                                <option value="Comments" >Comments</option>
                                <option value="Ask Question">Ask Question</option>
                                <option value="Bug Report">Bug Report</option>
                                <option value="Feature Request">Feature Request</option>
                                <option value="Account Delete Request">Account Delete Request</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12">
                            <label>Feedback Title</label>
                            <div class="form-group">
                                <input required type="text" class="form-control" placeholder="Title Here" name="title">
                            </div>
                        </div><br>
                        <div class="col-lg-12 col-md-12">
                            <label>Feedback Description</label>
                            <div class="form-group">
                                <textarea required cols="5" class="form-control" placeholder="Write Feedback in Detail" name="ddd"></textarea>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12">
                            <center>
                                <button name="feedback" class="btn btn-success btn-round btn-simple" type="submit">Submit</button>
                                <button class="btn btn-danger btn-round btn-simple" type="button">Cancel</button>
                            </center>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Jquery Core Js --> 
<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
<script src="assets/bundles/mainscripts.bundle.js"></script>
</body>
</html>
<?php include('includes/own.php'); ?>