﻿<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
    <?php include('includes/meta.php'); ?>
    <?php include('includes/title.php'); ?>
    <!-- Favicon-->
    <?php include('includes/favicon.php'); ?>
    <!-- Custom Css -->
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/authentication.css">
    <link rel="stylesheet" href="assets/css/color_skins.css">
</head>

<body class="theme-purple authentication sidebar-collapse">

<div class="page-header">
    <div class="page-header-image" style="background-image:url(reg/images/form-wizard-bg.jpg)"></div>
    <div class="container">
        <div class="col-md-12 content-center">
            <div class="card-plain">
                <form class="form" method="" action="#">
                    <div class="header">
                        <div class="logo-container">
                            <img src="logo.png" alt="">
                        </div>
                        <h5>Forgot Password?</h5>
                        <span>Enter your e-mail address below to reset your password.</span>
                    </div>
                    <div class="content">
                        <div class="input-group input-lg">
                            <input type="text" class="form-control" placeholder="Enter Email">
                            <span class="input-group-addon">
                                <i class="zmdi zmdi-email"></i>
                            </span>
                        </div>
                    </div>
                    <div class="footer text-center">
                        <a href="index.php" class="btn l-cyan btn-round btn-lg btn-block waves-effect waves-light">SUBMIT</a>
                        <h6 class="m-t-20"><a href="index.php" class="link">Remember Password?</a></h6>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

<!-- Jquery Core Js -->
<script src="assets/bundles/libscripts.bundle.js"></script>
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
<script>
   $(".navbar-toggler").on('click',function() {
    $("html").toggleClass("nav-open");
});
</script>
</body>
</html>