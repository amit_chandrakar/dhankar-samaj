<?php
    session_start();
    include('includes/check_user.php');
    include('includes/config.php');
?>
<!doctype html>
<html class="no-js " lang="en">
<head>
<?php include('includes/meta.php'); ?>
<?php include('includes/title.php'); ?>
<?php include('includes/favicon.php'); ?>
<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/footable-bootstrap/css/footable.bootstrap.min.css">
<link rel="stylesheet" href="assets/plugins/footable-bootstrap/css/footable.standalone.min.css">
<!-- Custom Css -->
<link rel="stylesheet" href="assets/css/main.css">
<link rel="stylesheet" href="assets/css/color_skins.css">
</head>

<body class="theme-purple">
<!-- Page Loader -->
<?php include('includes/preloader.php'); ?>

<!-- Overlay For Sidebars -->
<div class="overlay"></div>

<?php include('includes/top_navbar.php'); ?>
<?php include('includes/left_sidebar.php'); ?>



<section class="content contact">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Users
                <?php include('includes/sub_title.php'); ?>
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="dashboard.php"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
                    <li class="breadcrumb-item active">Users</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card action_bar">
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-6">
                                <label>Name</label>
                                <select class="form-control show-tick" name="search_marital_status">
                                    <option value="">--select--</option>
                                    <?php
                                    $sql=mysql_query("select * from user where utype!='A' order by fname",$conn);
                                    while ($row=mysql_fetch_assoc($sql)) 
                                    {
                                      ?>
                                       <option value="<?=$row['userid']?>"><?=$row['fname'],' ',$row['mname'],' ',$row['lname']?></option>
                                      <?php
                                    } ?>
                                </select>
                            </div>
                            <div class="col-lg-3 col-md-3 col-6">
                                <label>Email</label>
                                <select class="form-control show-tick" name="search_marital_status">
                                    <option value="">--select--</option>
                                    <?php
                                    $sql=mysql_query("select * from user where utype!='A' order by fname",$conn);
                                    while ($row=mysql_fetch_assoc($sql)) 
                                    {
                                      ?>
                                       <option value="<?=$row['userid']?>"><?=$row['email']?></option>
                                      <?php
                                    } ?>
                                </select>
                            </div>
                            <div class="col-lg-3 col-md-3 col-6" style="margin-top: 30px;">
                                <div class="input-group search">
                                    <button type="button" class="btn btn-default btn-simple btn-icon-mini btn-round hidden-sm-down">
                                    Search</i>
                                </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>           
        </div>
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="body table-responsive">
                        <table class="table table-hover m-b-0 c_list">
                            <thead>
                                <tr>
                                    <th>Name</th>                                    
                                    <th data-breakpoints="xs">Phone</th>
                                    <th data-breakpoints="xs sm md">Email</th>
                                    <th data-breakpoints="xs sm md">Address</th>
                                    <th data-breakpoints="xs sm md">Status</th>
                                    <th data-breakpoints="xs">Action</th>
                                </tr>
                            </thead>
                                <tbody>

                                    <?php
                                     $data = "SELECT * FROM user WHERE utype!='A'";
                                    $res = mysql_query($data,$conn);
                                    while ($row=mysql_fetch_assoc($res)) 
                                    { ?>
                                        <tr>
                                    <td>
                                        <img style="height:35px;width:35px;" src="<?php if(file_exists("uploads/".$row['userid']."/".$row['photo'])){echo "uploads/".$row['userid']."/".$row['photo'];}else{echo "assets/images/xs/avatar1.png";} ?> " class="rounded-circle">
                                        <!-- <img src="assets/images/xs/avatar1.jpg" class="rounded-circle avatar" alt=""> -->
                                        <span class="c_name"><?=$row['fname']," ",$row['lname']?></span>
                                    </td>
                                    <td>
                                        <span class="phone"><i class="zmdi zmdi-phone m-r-10"></i><?php
                                        $phone = $row['contact'];
                                        $numbers_only = preg_replace("/[^\d]/", "", $phone);
                                        echo preg_replace("/^1?(\d{3})(\d{3})(\d{4})$/", "$1-$2-$3", $numbers_only);
                                        
                                        ?></span>
                                    </td>
                                    <td>
                                        <span class="email"><a href="#"><i class="zmdi zmdi-email m-r-5"></i><?=$row['email']?></a></span>
                                    </td>
                                    <td>
                                        <address><i class="zmdi zmdi-pin"></i><?php if($row['location']==""){echo "Not Available";}else{echo $row['location'];}?></address>
                                    </td>
                                    <td>
                                        <span><?php if($row['status']!='A'){echo "<span class='badge badge-danger'>Not Varified</span>";}else{echo "<span class='badge badge-success'>Varified</span>";}?></span>
                                    </td>
                                    <td>
                                        <button onclick="javascript:location.href='user_desc.php?userid=<?=$row['userid']?>'" class="btn btn-default btn-icon btn-simple btn-icon-mini btn-round"><i class="zmdi zmdi-edit"></i></button>
                                        <!-- <button class="btn btn-default btn-icon btn-simple btn-icon-mini btn-round"><i class="zmdi zmdi-delete"></i></button> -->
                                    </td>
                                </tr>
                                  <?php } ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
  
    </div>
</section>
<!-- Jquery Core Js -->
<script src="assets/bundles/libscripts.bundle.js"></script>
<script src="assets/bundles/vendorscripts.bundle.js"></script>
<script src="assets/bundles/footable.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
<script src="assets/bundles/mainscripts.bundle.js"></script>
<script src="assets/js/pages/tables/footable.js"></script><!-- Custom Js -->
</body>
</html>